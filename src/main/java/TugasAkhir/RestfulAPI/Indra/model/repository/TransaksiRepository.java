package TugasAkhir.RestfulAPI.Indra.model.repository;

import TugasAkhir.RestfulAPI.Indra.model.entity.Transaksi;
import TugasAkhir.RestfulAPI.Indra.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransaksiRepository extends JpaRepository<Transaksi, Integer> {
    List<Transaksi> findByUser(User user);
}
