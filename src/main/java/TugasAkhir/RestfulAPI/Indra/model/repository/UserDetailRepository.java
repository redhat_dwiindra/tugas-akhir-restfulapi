package TugasAkhir.RestfulAPI.Indra.model.repository;

import TugasAkhir.RestfulAPI.Indra.model.entity.UserDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDetailRepository extends JpaRepository<UserDetail, Integer> {
}
