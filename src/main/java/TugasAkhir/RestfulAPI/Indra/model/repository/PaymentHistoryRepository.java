package TugasAkhir.RestfulAPI.Indra.model.repository;

import TugasAkhir.RestfulAPI.Indra.model.entity.PaymentHistory;
import TugasAkhir.RestfulAPI.Indra.model.entity.Transaksi;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PaymentHistoryRepository extends JpaRepository<PaymentHistory, Integer> {
    List<PaymentHistory> findByTransaksi(Transaksi transaksi);
}
