package TugasAkhir.RestfulAPI.Indra.model.repository;

import TugasAkhir.RestfulAPI.Indra.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsername(String username);
}
