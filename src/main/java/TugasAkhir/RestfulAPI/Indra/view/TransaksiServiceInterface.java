package TugasAkhir.RestfulAPI.Indra.view;

import TugasAkhir.RestfulAPI.Indra.model.entity.Transaksi;

import java.util.Map;

public interface TransaksiServiceInterface {
    public Map save(Transaksi transaksi);
    public Map updateStatus(Transaksi transaksi);
}
